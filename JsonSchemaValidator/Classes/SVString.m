//
//  SVString.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVString.h"
#import "SVJsonSchema.h"
#import "NSString+errorMessages.h"

@implementation SVString

+(NSString *)type
{
    return SVStringKey;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    if ( self )
    {
        self.format = dictionary[SVFormatKey];
    }
    return self;
}

-(id)validateJson:( NSString* )mustBeString errors:( NSMutableArray* )errors
{
    id validated = [super validateJson:mustBeString errors:errors];

    if ( validated && ![validated isKindOfClass:[NSString class]] )
    {
        [errors addObject:[NSString mustBeType:[self.class type] instead:[mustBeString class]]];
        return nil;
    }
    return validated;
}

-(NSMutableDictionary *)toJsonObject
{
    id items = [super toJsonObject];
    items[SVTypeKey] = SVStringKey;
    
    if (self.format)
    {
        items[SVFormatKey] = self.format;
    }
    return items;
}

-(id)instantiateValidatedJson:( NSString* )validatedJson
{
    if ( [self.format isEqualToString:SVURLKey] )
    {
        return [NSURL URLWithString:validatedJson];
    }
    return validatedJson;
}

-(instancetype)format:( NSString* )format
{
    self.format = format;
    return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"<%@ %@>", NSStringFromClass([self class]), [self toJsonObject]];
}

@end
