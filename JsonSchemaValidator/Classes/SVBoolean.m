//
//  SVBoolean.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVBoolean.h"
#import "SVTypes.h"

@implementation SVBoolean

+(NSString *)type
{
    return SVBooleanKey;
}

-(id)validateJson:(id)jsonObject errors:( NSMutableArray* )errors
{
    jsonObject = [super validateJson:jsonObject errors:errors];
    return [jsonObject isKindOfClass:[NSNumber class]] ? jsonObject : nil;
}

-(NSMutableDictionary *)toJsonObject
{
    id items = [super toJsonObject];
    items[SVTypeKey] = SVBooleanKey;
    return items;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@",[self toJsonObject]];
}

@end
