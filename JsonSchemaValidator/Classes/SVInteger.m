//
//  SVInteger.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVInteger.h"
#import "SVJsonSchema.h"
#import "NSObject+jsonTypes.h"

@implementation SVInteger

+(NSString *)type
{
    return SVIntegerKey;
}

-(id)validateJson:( NSNumber* )jsonObject errors:( NSMutableArray* )errors
{
    id validated = [super validateJson:jsonObject errors:errors];
    
    if ( validated && ![jsonObject isJsonInteger] )
    {
        [errors addObject:@"no floating point numbers are allowed"];
        return nil;
    }

    return validated;
}

-(NSMutableDictionary *)toJsonObject
{
    id items = [super toJsonObject];
    items[SVTypeKey] = SVIntegerKey;
    return items;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@",[self toJsonObject]];
}

@end
