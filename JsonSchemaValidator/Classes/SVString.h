//
//  SVString.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVType.h"

@interface SVString : SVType

@property (strong, nonatomic) NSString* format;

-(instancetype)format:( NSString* )format;

@end
