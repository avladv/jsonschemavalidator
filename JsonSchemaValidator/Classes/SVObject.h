//
//  SVObject.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVType.h"

@interface SVObject : SVType

@property (strong, nonatomic) Class objcClass;
@property (assign, nonatomic) BOOL hardNaming;
@property (nonatomic, strong) NSDictionary* properties;

-(instancetype)properties:( NSDictionary* )properties;
-(instancetype)hardNaming:( BOOL )hardNaming;

-(id)validateJsonObject:( NSDictionary* )object errors:(NSMutableArray *)errors;

@end
