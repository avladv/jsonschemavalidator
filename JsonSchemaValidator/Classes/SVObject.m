//
//  SVObject.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVObject.h"
#import "SVJsonSchema.h"
#import "NSObject+jsonTypes.h"
#import "NSString+errorMessages.h"

@implementation SVObject

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super initWithDictionary:dictionary];
    if ( self )
    {
        NSDictionary* properties = dictionary[SVPropertiesKey];
        if ( [properties isJsonObject] )
        {
            NSMutableDictionary* schemaProperties = [NSMutableDictionary dictionary];
            [properties enumerateKeysAndObjectsUsingBlock:^(id key, NSDictionary* schemaDictionary, BOOL* stop)
            {
                id schema = [SVType schemaWithDictionary:schemaDictionary];
                if ( schema )
                {
                    schemaProperties[key] = schema;
                }
            }];
            self.properties = schemaProperties;
        }
        else
        {
            self.properties = nil;
        }
        
        NSString* objcClass = dictionary[SVObjcClassKey];
        Class class = NSClassFromString(objcClass);
        if ( class )
        {
            self.objcClass = class;
        }
    }
    return self;
}

+(NSString *)type
{
    return SVObjectKey;
}

-(instancetype)properties:(NSDictionary *)properties
{
    self.properties = properties;
    return self;
}

-(instancetype)hardNaming:(BOOL)hardNaming
{
    self.hardNaming = hardNaming;
    return self;
}

-(id)validateJsonObject:( NSDictionary* )object errors:(NSMutableArray *)errors
{
    object = [super validateJson:object errors:errors];
    
    if ( object && ![object isKindOfClass:[NSDictionary class]] )
    {
        [errors addObject:[NSString mustBeType:[self class].type instead:[object class]]];
        return nil;
    }

    NSMutableDictionary* outJson = [NSMutableDictionary dictionary];
    
    if ( !self.hardNaming )
    {
        NSMutableDictionary* objectWithLowercase = [NSMutableDictionary dictionaryWithCapacity:object.count];
        [object enumerateKeysAndObjectsUsingBlock:^(NSString* key, id obj, BOOL* stop)
         {
             id newKey = [[key lowercaseString] stringByReplacingOccurrencesOfString:@"_" withString:@""];
             objectWithLowercase[newKey] = obj;
         }];
        object = objectWithLowercase;
    }
    
    NSMutableSet* jsonProperties = [NSMutableSet setWithArray:[object allKeys]];
    
    NSMutableArray* propertiesValidationErrors = [NSMutableArray array];
    
    [self.properties enumerateKeysAndObjectsUsingBlock:^(id schemaKey, SVType* schema, BOOL* stop)
     {
         id preparedKey = self.hardNaming ? schemaKey : [schemaKey lowercaseString];
         
         [jsonProperties removeObject:preparedKey];
         
         id jsonVal = self.hardNaming ? object[schemaKey] : object[[schemaKey lowercaseString]];
         
         NSMutableArray* propertyErrors = [NSMutableArray array];
         id validated = [schema validateJson:jsonVal errors:propertyErrors];
         
         if ( validated )
         {
             outJson[schemaKey] = validated;
         }
         else if ( propertyErrors.count )
         {
             propertyErrors[0] = [NSString stringWithFormat:@"%@ %@", schemaKey, propertyErrors[0]];
             [propertiesValidationErrors addObjectsFromArray:propertyErrors];
         }
     }];
    
    if ( propertiesValidationErrors.count )
    {
        [errors addObjectsFromArray:propertiesValidationErrors];
        return nil;
    }

    for ( id key in jsonProperties)
        outJson[key] = object[key];

    return outJson;
}

-(id)validateJson:( NSDictionary* )object errors:( NSMutableArray* )errors
{
    if ( self.objcClass )
    {
        return [self.objcClass validateJson:object schema:self errors:errors];
    }
    else
    {
        return [self validateJsonObject:object errors:errors];
    }
}

-(id)instantiateValidatedJson:( NSDictionary* )validatedJson
{
    if ( self.objcClass )
    {
        return [self.objcClass instanceFromJson:validatedJson schema:self];
    }
    else
    {
        NSMutableDictionary* validatedOut =[NSMutableDictionary dictionary];
        [self.properties enumerateKeysAndObjectsUsingBlock:^(id key, SVType* schema, BOOL* stop)
         {
             id property = validatedJson[key];
             if ( property )
             {
                 id validatedProperty = [schema instantiateValidatedJson:property];
                 validatedOut[key] = validatedProperty;
             }
         }];
        return validatedOut;
    }
}

-(NSDictionary *)toJsonObject
{
    id items = [super toJsonObject];
    items[SVTypeKey] = SVObjectKey;

    if ( self.properties )
    {
        NSMutableDictionary* properties = [NSMutableDictionary dictionaryWithCapacity:self.properties.count];
        [self.properties enumerateKeysAndObjectsUsingBlock:^(id key, SVType* schema, BOOL* stop)
        {
            properties[key] = [schema toJsonObject];
        }];
        
        items[SVPropertiesKey] = properties;
    
    }
    
    if ( self.objcClass )
        items[SVObjcClassKey] = NSStringFromClass( self.objcClass );
    
    return items;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@",[self toJsonObject]];
}

@end
