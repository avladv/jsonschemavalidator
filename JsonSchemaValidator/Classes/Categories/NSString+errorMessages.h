//
//  NSString+errorMessages.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 22.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (errorMessages)

+(id)mustBeType:( NSString* )mustBeType instead:( Class )instead;
@end
