//
//  NSString+errorMessages.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 22.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "NSString+errorMessages.h"

@implementation NSString (errorMessages)

+(id)mustBeType:( NSString* )mustBeType instead:( Class )instead
{
    return [self stringWithFormat:@"must be a type '%@' but class is '%@'", mustBeType, instead];
}


@end

