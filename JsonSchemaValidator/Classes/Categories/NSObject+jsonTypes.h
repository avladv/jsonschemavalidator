//
//  NSObject+jsonTypes.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/23/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SVType;

@interface NSObject (jsonTypes)

-(BOOL)isJsonInteger;
-(BOOL)isJsonNumber;
-(BOOL)isJsonBoolean;
-(BOOL)isJsonNull;
-(BOOL)isJsonArray;
-(BOOL)isJsonObject;
-(BOOL)isJsonString;

+(SVType*)jsonSchema;
-(SVType*)jsonSchema;
+(SVType*)jsonSchemaForProperty:(NSString*)property;

-(id)initWithJson:( NSDictionary* )json schema:( SVType* )schema;
+(id)instanceFromJson:( NSDictionary* )json schema:( SVType* )schema;

+(id)validateJson:( id )jsonObject schema:( SVType* )schema errors:( NSMutableArray* )errors;

+(void)prepareJsonSchema:( SVType* )schema forProperty:( NSString* )property;

@end
