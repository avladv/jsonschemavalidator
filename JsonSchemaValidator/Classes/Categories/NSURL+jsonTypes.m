//
//  NSURL+jsonTypes.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 26.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "NSURL+jsonTypes.h"
#import "NSObject+jsonTypes.h"
#import "SVJsonSchema.h"

@implementation NSURL (jsonTypes)

+(SVType *)jsonSchema
{
    SVString* string = [SVType string];
    string.format = SVURLKey;
    return string;
}

@end
