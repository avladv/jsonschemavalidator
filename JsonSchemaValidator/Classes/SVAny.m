//
//  SVAny.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVAny.h"
#import "SVTypes.h"

@implementation SVAny

+(NSString *)type
{
    return SVAnyKey;
}

-(id)validateJson:( id )jsonObject errors:( NSMutableArray* )errors
{
    return jsonObject;
}

-(instancetype)enumValues:(NSArray *)enumValues
{
    [self doesNotRecognizeSelector:_cmd];
    return self;
}

-(NSDictionary *)toJsonObject
{
    id json = [super toJsonObject];
    json[SVTypeKey] = SVAnyKey;
    return json;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@",[self toJsonObject]];
}


@end
