//
//  SVJsonSchemaEnumTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/22/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonSchemaEnumTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonSchemaEnumTests

-(void)testEnum
{
    NSString* key = @"key";
    SVObject* object = [[SVType object] properties:@{ key: [[SVType string] enumValues:@[@"1", @"2", @"3"]] }];
    
    NSError* error = nil;
    
    id fineObject = @{key: @"1"};
    STAssertNotNil( [object validateJson:fineObject error:&error] , nil);
    STAssertNil( error, nil);
}

-(void)testEnumFail
{
    NSString* key = @"key";
    SVObject* object = [[SVType object] properties:@{ key: [[SVType string] enumValues:@[@"1", @"2", @"3"]] }];
    
    NSError* error = nil;
    
    id badObject = @{key: @"qwerty"};
    
    STAssertNil( [object validateJson:badObject error:&error], nil);
    STAssertNotNil( error, nil);
}

-(void)testEnumWrongType
{
    STAssertThrows( [[SVType string] enumValues:@[ @1 ]] , nil);
}

@end
