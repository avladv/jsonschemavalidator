//
//  SVAdditionalNSObjectTypesTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 26.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVNSURLTypeTests.h"
#import "SVJsonSchema.h"

@interface SVAdditionalProperyTypes : NSObject

@property (strong, nonatomic) NSURL* url;

@end

@implementation SVAdditionalProperyTypes

@end

@interface SVNSURLTypeTests()

@property (strong, nonatomic) NSString* key;
@property (strong, nonatomic) SVObject* schema;

@end

@implementation SVNSURLTypeTests

-(void)setUp
{
    [super setUp];
    self.key = @"url";
    self.schema = (SVObject*)[SVAdditionalProperyTypes jsonSchema];
}

-(void)tearDown
{
    [super tearDown];
    self.schema = nil;
}

-(void)testNSURL
{
    NSError* error = nil;
    id url = @{self.key:@"http://google.com"};
    id validated = [self.schema validateJson:url error:&error];
    STAssertEqualObjects(validated, url, nil);
}

-(void)testWrongType
{
    NSError* error = nil;
    id url = @{self.key:@1};
    id validated = [self.schema validateJson:url error:&error];
    STAssertNil(validated, nil);
    STAssertNotNil(error, nil);
}

-(void)testDafaultValue
{
    id defaultVal = @"qwerty";
    
    SVType* type = self.schema.properties[self.key];
    type.defaultVal = defaultVal;
    
    NSError* error = nil;
    id validated = [self.schema validateJson:@{} error:&error];
    
    id expectedResult = @{self.key:defaultVal};
    STAssertEqualObjects(validated, expectedResult, nil);
}

-(void)testNilValue
{
    NSError* error = nil;
    STAssertNil([self.schema validateJson:@{self.key:[NSNull null]} error:&error], nil);
    STAssertNotNil(error, nil);
}

-(void)testEnum
{
    [self.schema.properties[self.key] enumValues: @[@"1", @"2"]];
    
    NSError* error = nil;
    id input = @{self.key:@"1"};
    
    STAssertEqualObjects([self.schema validateJson:input error:&error], input, nil);
    STAssertNil(error, nil);
    
    error = nil;
    
    id input2 = @{self.key:@"3"};
    STAssertNil( [self.schema validateJson:input2 error:&error], nil );
    STAssertNotNil(error, nil);
}

-(void)testInstanciate
{
    self.schema = (SVObject*)[SVAdditionalProperyTypes jsonSchemaForProperty:@"url"];
    id url =  @"google.com";
    STAssertEqualObjects([self.schema instantiateValidatedJson:url], [NSURL URLWithString:url], nil);
}

@end
