//
//  SVTypeCreationTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/21/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonSchemaTypeCreationTests.h"
#import "SVJsonSchema.h"

@implementation SVJsonSchemaTypeCreationTests

-(void)testBaseWithEmpty
{
    SVType* type = [[SVType alloc] initWithDictionary:@{}];
    STAssertNil(type.title, nil);
    STAssertNil(type.schemaDescription, nil);
    STAssertNil(type.schemaId, nil);
    STAssertNil(type.schema, nil);
}

-(void)testID
{
    NSString* schemaId = @"qwerty";
    SVType* type = [[SVType alloc] initWithDictionary:@{SVIDKey:schemaId}];
    STAssertEqualObjects(type.schemaId, schemaId, nil);
}

-(void)testTitle
{
    NSString* title = @"qwerty";
    SVType* type = [[SVType alloc] initWithDictionary:@{SVTitleKey:title}];
    STAssertEqualObjects(type.title, title, nil);
}

-(void)testDescription
{
    NSString* description = @"qwerty";
    SVType* type = [[SVType alloc] initWithDictionary:@{SVDescriptionKey:description}];
    STAssertEqualObjects(type.schemaDescription, description, nil);
}

-(void)testSchema
{
    NSString* schema = @"qwerty";
    SVType* type = [[SVType alloc] initWithDictionary:@{SVSchemaKey:schema}];
    STAssertEqualObjects(type.schema, schema, nil);
}

-(void)testDefaultValue
{
    SVType* schema = [[SVType alloc] initWithDictionary:@{SVDefaultKey:@YES}];
    STAssertEqualObjects(schema.defaultVal, @YES, nil);
}

-(void)testRequired
{
    SVType* schema = [[SVType alloc] initWithDictionary:@{SVRequiredKey:@YES}];
    STAssertEquals(schema.required, YES, nil);
}

-(void)testRequiredWrongType
{
    SVType* schema = [[SVType alloc] initWithDictionary:@{SVRequiredKey:[NSNull null]}];
    STAssertFalse(schema.required, nil);
}

-(void)testEnum
{
    id enumVal = @[@YES, @NO];
    SVType* schema = [[SVType alloc] initWithDictionary:@{SVEnumKey:enumVal}];
    STAssertEqualObjects(schema.enumValues, enumVal, nil);
}

-(void)testEnumWrongType
{
    id enumVal = @YES;
    SVType* schema = [[SVType alloc] initWithDictionary:@{SVEnumKey:enumVal}];
    STAssertEqualObjects(schema.enumValues, nil, nil);
    
}

@end
