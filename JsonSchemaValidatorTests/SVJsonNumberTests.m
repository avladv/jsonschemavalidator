//
//  SVJsonNumberTests.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/22/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVJsonNumberTests.h"
#import "SVJsonSchema.h"

@interface SVJsonNumberTests ()

@property (strong, nonatomic) SVNumber* number;

@end

@implementation SVJsonNumberTests

-(void)setUp
{
    [super setUp];
    self.number = [SVType number];
}

-(void)tearDown
{
    [super tearDown];
    self.number = nil;
}

-(void)testMinMaxConflicts
{
    STAssertThrows( [[self.number minimum:@(10)] maximum:@(0)], nil);
}

-(void)testMaxMinConflicts
{
    STAssertThrows( [[self.number maximum:@(0)] minimum:@(10)], nil);
}

-(void)testMinimum
{
    [self.number minimum:@(10)];
    NSError* error = nil;
    STAssertNotNil([self.number validateJson:@(15) error:&error], nil);
    STAssertNil(error, nil);
}

-(void)testMinimumFalse
{
    self.number = [self.number minimum:@(10)];
    NSError* error = nil;
    STAssertNil([self.number validateJson:@(5) error:&error], nil);
    STAssertNotNil(error, nil);
}

-(void)testExclusiveMinimum
{
    [[self.number minimum:@(10)] exclusiveMinimum:YES];
    NSError* error = nil;
    STAssertNil([self.number validateJson:@(10) error:&error], nil);
    STAssertNotNil(error, nil);
}

-(void)testExclusiveMinimumFalse
{
    [self.number minimum:@(10)];
    NSError* error = nil;
    STAssertNotNil([self.number validateJson:@(10) error:&error], nil);
    STAssertNil(error, nil);
}

-(void)testMaximum
{
    [self.number maximum:@(10)];
    NSError* error = nil;
    STAssertNotNil([self.number validateJson:@(5) error:&error], nil);
    STAssertNil(error, nil);
}

-(void)testMaximumFalse
{
    self.number = [self.number maximum:@(10)];
    NSError* error = nil;
    STAssertNil([self.number validateJson:@(15) error:&error], nil);
    STAssertNotNil(error, nil);
}

-(void)testExclusiveMaximum
{
    [[self.number maximum:@(10)] exclusiveMaximum:YES];
    NSError* error = nil;
    STAssertNil([self.number validateJson:@(10) error:&error], nil);
    STAssertNotNil(error, nil);
}

-(void)testExclusiveMaximumFalse
{
    [self.number maximum:@(10)];
    NSError* error = nil;
    STAssertNotNil([self.number validateJson:@(10) error:&error], nil);
    STAssertNil(error, nil);
}

@end
