//
//  SVTweet.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 23.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVTweet.h"

@implementation SVTweet

-(NSString *)description
{
    return [NSString stringWithFormat:@"<%@ user:%@ | test:%@>", NSStringFromClass([self class]), self.fromUserName, self.text];
}

@end
