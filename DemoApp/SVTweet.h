//
//  SVTweet.h
//  JsonSchemaValidator
//
//  Created by Max Lunin on 23.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SVTweet : NSObject

@property (strong, nonatomic) NSString* text;
@property (strong, nonatomic) NSString* fromUserName;
@property (strong, nonatomic) NSString* profileImageUrl;

@end



//"created_at" = "Thu, 23 May 2013 17:12:11 +0000";
//"from_user" = DevonMinaj;
//"from_user_id" = 99591789;
//"from_user_id_str" = 99591789;
//"from_user_name" = "Devon.";
//geo = "<null>";
//id = 337616963847192577;
//"id_str" = 337616963847192577;
//"in_reply_to_status_id" = 337616914257952768;
//"in_reply_to_status_id_str" = 337616914257952768;
//"iso_language_code" = tl;
//metadata =             {
//    "result_type" = recent;
//};
//"profile_image_url" = "http://a0.twimg.com/profile_images/3699415607/790c1b86d34341d29f3d3334170265a2_normal.jpeg";
//"profile_image_url_https" = "https://si0.twimg.com/profile_images/3699415607/790c1b86d34341d29f3d3334170265a2_normal.jpeg";
//source = "&lt;a href=&quot;http://www.tweetdeck.com&quot;&gt;TweetDeck&lt;/a&gt;";
//text = "@fucktyler ITS FINE DUDE FUCK IT!1111111111111111111111111";
//"to_user" = fucktyler;
//"to_user_id" = 166747718;
//"to_user_id_str" = 166747718;
//"to_user_name" = "Tyler, The Creator";
